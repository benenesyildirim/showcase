package com.benenesyildirim.vitrin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.responsemodel.Shop
import kotlinx.android.synthetic.main.editor_shops_slider_row_design.view.*

class EditorShopsViewPagerAdapter(private val context: Context, private val editorShopList: List<Shop>) : PagerAdapter() {
    private var lastPosition: Int = 0

    override fun getCount(): Int {
        return editorShopList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.editor_shops_slider_row_design, null)

        Glide.with(view).load(editorShopList[position].logo.medium.url).circleCrop().into(view.shop_image_editor_shops_viewpage)
        Glide.with(view).load(editorShopList[position].popular_products[0].images[0].medium.url).fitCenter().into(view.product_image_one_editor_shops_viewpage)
        Glide.with(view).load(editorShopList[position].popular_products[1].images[0].medium.url).fitCenter().into(view.product_image_two_editor_shops_viewpage)
        Glide.with(view).load(editorShopList[position].popular_products[2].images[0].medium.url).fitCenter().into(view.product_image_three_editor_shops_viewpage)

        view.title_editor_shops_viewpager.text = editorShopList[position].name
        view.subtitle_editor_shops_viewpager.text = editorShopList[position].definition

        val vp = container as ViewPager
        vp.addView(view, 0)

        val animation: Animation = AnimationUtils.loadAnimation(context, if (position > lastPosition) R.anim.left_from_right else R.anim.right_from_left)
        view.startAnimation(animation)
        lastPosition = position

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}
