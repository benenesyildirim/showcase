package com.benenesyildirim.vitrin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.responsemodel.Shop
import kotlinx.android.synthetic.main.new_shops_row_design.view.*

class NewShopsViewPagerAdapter (private val context: Context, private val newShopList: List<Shop>) : PagerAdapter() {
    private var lastPosition: Int = 0

    override fun getCount(): Int {
        return newShopList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.new_shops_row_design, null)

        if (newShopList[position].cover != null){
            Glide.with(view).load(newShopList[position].cover.url).circleCrop().into(view.profile_photo_new_shops)
            Glide.with(view).load(newShopList[position].cover.url).fitCenter().into(view.header_image_new_shops)
        }

        view.title_new_shops.text = newShopList[position].name
        view.subtitle_new_shops.text = newShopList[position].definition
        view.count_new_shops.text = newShopList[position].product_count.toString() + " ÜRÜN"

        val vp = container as ViewPager
        vp.addView(view, 0)

        val animation: Animation = AnimationUtils.loadAnimation(context, if (position > lastPosition) R.anim.left_from_right else R.anim.right_from_left)
        view.startAnimation(animation)
        lastPosition = position

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}
