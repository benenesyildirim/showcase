package com.benenesyildirim.vitrin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.responsemodel.Product


class ProductsAdapter(val productList: List<Product>) : RecyclerView.Adapter<ProductsAdapter.ModelViewHolder>() {
    private var lastPosition: Int = 0

    class ModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.product_row_title)
        private val seller: TextView = view.findViewById(R.id.product_row_subtitle)
        private val price: TextView = view.findViewById(R.id.product_row_price)
        private val image: ImageView = view.findViewById(R.id.product_row_image)

        fun bindItems(item: Product) {
            title.text = item.title
            seller.text = item.difference
            price.text = item.price.toString() + " TL"
            Glide.with(itemView).load(item.images[0].thumbnail.url).fitCenter().into(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.products_rv_row_design, parent, false)
        return ModelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        holder.bindItems(productList.get(position))
        val animation: Animation = AnimationUtils.loadAnimation(holder.itemView.context, if (position > lastPosition) R.anim.left_from_right else R.anim.right_from_left)
        holder.itemView.startAnimation(animation)
        lastPosition = position
    }

    override fun onViewDetachedFromWindow(holder: ModelViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.itemView.clearAnimation();
    }
}