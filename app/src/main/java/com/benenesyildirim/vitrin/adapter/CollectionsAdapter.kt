package com.benenesyildirim.vitrin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.responsemodel.Collection

class CollectionsAdapter(val collectionsList: List<Collection>) : RecyclerView.Adapter<CollectionsAdapter.ModelViewHolder>() {
    private var lastPosition: Int = 0

    class ModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.collections_row_title)
        private val subtitle: TextView = view.findViewById(R.id.collections_row_subtitle)
        private val image: ImageView = view.findViewById(R.id.collections_row_image)

        fun bindItems(item: Collection) {
            title.text = item.title
            subtitle.text = item.definition
            Glide.with(itemView).load(item.cover.thumbnail.url).fitCenter().into(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.collections_rv_row_design, parent, false)
        return ModelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return collectionsList.size
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        holder.bindItems(collectionsList[position])

        val animation: Animation = AnimationUtils.loadAnimation(holder.itemView.context, if (position > lastPosition) R.anim.up_from_bottom else R.anim.down_from_top)
        holder.itemView.startAnimation(animation)
        lastPosition = position
    }

    override fun onViewDetachedFromWindow(holder: ModelViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.itemView.clearAnimation();
    }
}