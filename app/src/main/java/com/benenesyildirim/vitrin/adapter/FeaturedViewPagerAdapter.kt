package com.benenesyildirim.vitrin.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.responsemodel.Featured
import kotlinx.android.synthetic.main.featured_slider_row_design.view.*


class FeaturedViewPagerAdapter(private val context: Context, private val featuredList: List<Featured>) : PagerAdapter() {
    private var lastPosition: Int = 0

    override fun getCount(): Int {
        return featuredList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.featured_slider_row_design, null)

        Glide.with(view).load(featuredList[position].cover.thumbnail.url).fitCenter().into(view.image_featured_viewpager)
        view.title_featured_viewpager.text = featuredList[position].title.toUpperCase()
        view.subtitle_featured_viewpager.text = featuredList[position].sub_title.toUpperCase()

        val vp = container as ViewPager
        vp.addView(view, 0)

        val animation: Animation = AnimationUtils.loadAnimation(context, if (position > lastPosition) R.anim.left_from_right else R.anim.right_from_left)
        view.startAnimation(animation)
        lastPosition = position

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}
