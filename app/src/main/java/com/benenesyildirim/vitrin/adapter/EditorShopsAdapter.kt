package com.benenesyildirim.vitrin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.responsemodel.Shop


class EditorShopsAdapter(val shopList: List<Shop>) : RecyclerView.Adapter<EditorShopsAdapter.ModelViewHolder>() {
    private var lastPosition: Int = 0

    class ModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.title_editor_shops)
        private val subtitle: TextView = view.findViewById(R.id.subtitle_editor_shops)
        private val count: TextView = view.findViewById(R.id.count_editor_shops)
        private val headerImage: ImageView = view.findViewById(R.id.header_image_editor_shops)
        private val profilePhoto: ImageView = view.findViewById(R.id.profile_photo_editor_shops)

        fun bindItems(item: Shop) {
            title.text = item.name
            subtitle.text = item.definition
            count.text = item.product_count.toString() + " ÜRÜN"
            if (item.cover != null) Glide.with(itemView).load(item.cover.url).fitCenter().into(headerImage)
            if (item.logo != null) Glide.with(itemView).load(item.logo.url).circleCrop().into(profilePhoto)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.editor_shops_rv_row_design, parent, false)
        return ModelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shopList.size
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        holder.bindItems(shopList[position])
        val animation: Animation = AnimationUtils.loadAnimation(holder.itemView.context, if (position > lastPosition) R.anim.up_from_bottom else R.anim.down_from_top)
        holder.itemView.startAnimation(animation)
        lastPosition = position
    }

    override fun onViewDetachedFromWindow(holder: ModelViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.itemView.clearAnimation();
    }
}