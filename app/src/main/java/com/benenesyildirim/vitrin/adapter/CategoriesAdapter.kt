package com.benenesyildirim.vitrin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.responsemodel.Category

class CategoriesAdapter(val categoriesList: List<Category>) : RecyclerView.Adapter<CategoriesAdapter.ModelViewHolder>() {
    private var lastPosition: Int = 0

    class ModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.categories_row_title)
        private val image: ImageView = view.findViewById(R.id.categories_row_image)

        fun bindItems(item: Category) {
            title.text = item.name.toUpperCase()
            Glide.with(itemView).load(item.cover.medium.url).fitCenter().into(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.categories_rv_row_design, parent, false)
        return ModelViewHolder(view)
    }

    override fun getItemCount(): Int {
        return categoriesList.size
    }

    override fun onBindViewHolder(holder: ModelViewHolder, position: Int) {
        holder.bindItems(categoriesList.get(position))

        val animation: Animation = AnimationUtils.loadAnimation(holder.itemView.context, if (position > lastPosition) R.anim.left_from_right else R.anim.right_from_left)
        holder.itemView.startAnimation(animation)
        lastPosition = position
    }

    override fun onViewDetachedFromWindow(holder: ModelViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.itemView.clearAnimation();
    }
}