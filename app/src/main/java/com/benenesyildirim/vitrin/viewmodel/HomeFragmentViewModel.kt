package com.benenesyildirim.vitrin.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.benenesyildirim.vitrin.model.HomeFragmentModel
import com.benenesyildirim.vitrin.refrofit.responsemodel.MainData

class HomeFragmentViewModel(application: Application) : AndroidViewModel(application) {

    private val homeActivityModel = HomeFragmentModel(application)
    val showProgress: LiveData<Boolean>
    val data: LiveData<MainData>

    init {
        this.showProgress = homeActivityModel.showProgress
        this.data = homeActivityModel.data
    }

    fun getData() {
        homeActivityModel.getData()
    }

    fun setProgress(value: Boolean) {
        homeActivityModel.setProgress(value)
    }
}