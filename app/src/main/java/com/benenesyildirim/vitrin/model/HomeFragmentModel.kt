package com.benenesyildirim.vitrin.model

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.refrofit.ApiBaseCall
import com.benenesyildirim.vitrin.refrofit.CallNetwork
import com.benenesyildirim.vitrin.refrofit.responsemodel.MainData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragmentModel(val application: Application) {

    var showProgress = MutableLiveData<Boolean>()
    var data = MutableLiveData<MainData>()
    private val dataService = ApiBaseCall.getData().create(CallNetwork::class.java)

    fun setProgress(value: Boolean) {
        showProgress.value = value
    }

    fun getData() {
        dataService.getDataList().enqueue(object : Callback<MainData> {
            override fun onResponse(call: Call<MainData>, response: Response<MainData>) {
                showProgress.value = false
                data.value = response.body()
            }

            override fun onFailure(call: Call<MainData>, t: Throwable) {
                showProgress.value = false
                Toast.makeText(application, application.getString(R.string.check_internet), Toast.LENGTH_SHORT).show()
            }

        })
    }
}