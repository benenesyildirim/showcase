package com.benenesyildirim.vitrin.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.adapter.EditorShopsAdapter
import com.benenesyildirim.vitrin.refrofit.responsemodel.Shop
import kotlinx.android.synthetic.main.fragment_all_editor_shops.view.*
import kotlinx.android.synthetic.main.fragment_all_new_shops.view.*

class AllNewShopsFragment : Fragment() {
    private lateinit var newShopList: List<Shop>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_all_new_shops, container, false)

        if (arguments != null) {
            val shopListJson = arguments?.getString("shops", "")
            newShopList = Gson().fromJson(shopListJson, Array<Shop>::class.java).toList()

            setProductList(view)
        }

        view.back_btn_all_new_shops.setOnClickListener {
            Navigation.findNavController(view).popBackStack()
        }

        return view
    }

    private fun setProductList(view: View) {
        view.all_new_shops_rv.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        view.all_new_shops_rv.adapter = EditorShopsAdapter(newShopList)
    }
}