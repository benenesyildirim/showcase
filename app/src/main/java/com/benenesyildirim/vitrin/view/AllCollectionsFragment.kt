package com.benenesyildirim.vitrin.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.adapter.CollectionsAdapter
import com.benenesyildirim.vitrin.refrofit.responsemodel.Collection
import kotlinx.android.synthetic.main.fragment_all_collections.view.*
import kotlinx.android.synthetic.main.fragment_all_products.view.*

class AllCollectionsFragment : Fragment() {
    private lateinit var collectionsList: List<Collection>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_all_collections, container, false)

        if (arguments != null) {
            val collectionListJson = arguments?.getString("collections", "")
            collectionsList = Gson().fromJson(collectionListJson, Array<Collection>::class.java).toList()

            setCollectionList(view)
        }

        view.back_btn_all_collections.setOnClickListener {
            Navigation.findNavController(view).popBackStack()
        }
        return view
    }

    private fun setCollectionList(view: View?) {
        view?.all_collections_rv?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        view?.all_collections_rv?.adapter = CollectionsAdapter(collectionsList)
    }
}