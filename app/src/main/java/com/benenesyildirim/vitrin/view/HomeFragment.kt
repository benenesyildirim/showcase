package com.benenesyildirim.vitrin.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.os.Bundle
import android.os.Handler
import android.speech.RecognizerIntent
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout.TabLayoutOnPageChangeListener
import com.google.gson.Gson
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.adapter.*
import com.benenesyildirim.vitrin.refrofit.responsemodel.*
import com.benenesyildirim.vitrin.refrofit.responsemodel.Collection
import com.benenesyildirim.vitrin.viewmodel.HomeFragmentViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.*


class HomeFragment : Fragment() {

    private var doubleBackToExitPressedOnce: Boolean = false
    private lateinit var recognizedText: String
    private lateinit var homeViewModel: HomeFragmentViewModel
    private lateinit var mainData: MainData

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        backPressHandler()
        homeViewModel = ViewModelProvider(this).get(HomeFragmentViewModel::class.java)
        homeViewModel.setProgress(true)
        homeViewModel.getData()

        homeViewModel.showProgress.observe(viewLifecycleOwner, Observer {
            if (it) {
                progress_bar_home.visibility = VISIBLE
                swipe_refresh_layout.visibility = GONE
            } else {
                progress_bar_home.visibility = GONE
                swipe_refresh_layout.visibility = VISIBLE
            }
        })
        homeViewModel.data.observe(viewLifecycleOwner, {
            mainData = it
            view.swipe_refresh_layout.isRefreshing = false
            setFeaturedViewPager(it[0].featured)
            setProductsList(it[1].products)
            setCategoriesList(it[2].categories)
            setCollectionsList(it[3].collections)
            setEditorShopsViewPager(it[4].shops)
            setNewShopsViewPager(it[5].shops)
        })


        setClickListeners(view)
        setSmoothScrollForFeaturedViewPager(view)
        setSmoothScrollForProductList(view)
        editorPageSetBackground(view)

        view.swipe_refresh_layout.setOnRefreshListener {
            homeViewModel.getData()
        }

        return view
    }

    private fun setSmoothScrollForProductList(view: View) {
        view.products_rv_home.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)

                if(newState == SCROLL_STATE_DRAGGING) view.swipe_refresh_layout.isEnabled = false

                if(newState == SCROLL_STATE_IDLE) view.swipe_refresh_layout.isEnabled = true
            }
        })

    }

    private fun setSmoothScrollForFeaturedViewPager(view: View) {
        view.featured_viewpager.addOnPageChangeListener(object : TabLayoutOnPageChangeListener(view.featured_viewpager_dots) {
            override fun onPageScrollStateChanged(state: Int) {
                if (view.swipe_refresh_layout != null && !view.swipe_refresh_layout.isRefreshing) {
                    view.swipe_refresh_layout.isEnabled = state == ViewPager.SCROLL_STATE_IDLE
                }
            }
        })
    }

    private fun editorPageSetBackground(view: View) {
        view.editor_shops_viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                Glide.with(view).load(mainData[4].shops[position].cover.thumbnail.url).fitCenter().into(view.editor_shops_background_image)

                val colorMatrix = ColorMatrix()
                colorMatrix.setSaturation(0f)
                val filter = ColorMatrixColorFilter(colorMatrix)
                view.editor_shops_background_image.colorFilter = filter
            }

            override fun onPageSelected(position: Int) {

            }

        })
    }

    private fun setClickListeners(view: View) {
        view.speech_btn_home.setOnClickListener {
            startSpeechRecognition()
        }

        view.products_all_tv.setOnClickListener {
            val bundle = Bundle()
            val jsonString = Gson().toJson(mainData[1].products)
            bundle.putString("products", jsonString)
            Navigation.findNavController(view).navigate(R.id.action_homeFragment_to_allProductsFragment, bundle)
        }

        view.collections_all_tv.setOnClickListener {
            val bundle = Bundle()
            val jsonString = Gson().toJson(mainData[3].collections)
            bundle.putString("collections", jsonString)
            Navigation.findNavController(view).navigate(R.id.action_homeFragment_to_allCollectionsFragment, bundle)
        }

        view.editor_shops_all_tv.setOnClickListener {
            val bundle = Bundle()
            val jsonString = Gson().toJson(mainData[4].shops)
            bundle.putString("shops", jsonString)
            Navigation.findNavController(view).navigate(R.id.action_homeFragment_to_allEditorShopsFragment, bundle)
        }

        view.new_shops_all_tv.setOnClickListener {
            val bundle = Bundle()
            val jsonString = Gson().toJson(mainData[5].shops)
            bundle.putString("shops", jsonString)
            Navigation.findNavController(view).navigate(R.id.action_homeFragment_to_allNewShopsFragment, bundle)
        }
    }

    private fun setNewShopsViewPager(shops: List<Shop>) {
        val newShopsViewPagerAdapter = NewShopsViewPagerAdapter(requireContext(), shops)
        new_shops_viewpager.adapter = newShopsViewPagerAdapter
    }

    private fun setEditorShopsViewPager(shops: List<Shop>) {
        val editorShopsViewPagerAdapter = EditorShopsViewPagerAdapter(requireContext(), shops)
        editor_shops_viewpager.adapter = editorShopsViewPagerAdapter
    }

    private fun setCollectionsList(collections: List<Collection>) {
        collections_rv_home.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        collections_rv_home.adapter = CollectionsAdapter(collections)
    }

    private fun setCategoriesList(categories: List<Category>) {
        categories_rv_home.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        categories_rv_home.adapter = CategoriesAdapter(categories)
    }

    private fun setProductsList(products: List<Product>) {
        products_rv_home.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        products_rv_home.adapter = ProductsAdapter(products)
    }

    private fun setFeaturedViewPager(it: List<Featured>) {
        val featuredViewPagerAdapter = FeaturedViewPagerAdapter(requireContext(), it)
        featured_viewpager.adapter = featuredViewPagerAdapter
        featured_viewpager_dots.setupWithViewPager(featured_viewpager)
    }

    private fun startSpeechRecognition() {
        val sttIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        sttIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        sttIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        sttIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak now!")
        try {
            startActivityForResult(sttIntent, 1)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    if (!result.isNullOrEmpty()) {
                        recognizedText = result[0]
                        view?.search_bar_et_home?.setText(recognizedText)
                    }
                }
            }
        }
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (doubleBackToExitPressedOnce) {
                    activity!!.finishAffinity()
                }

                this@HomeFragment.doubleBackToExitPressedOnce = true
                Toast.makeText(context, getString(R.string.r_u_sure_to_exit), Toast.LENGTH_SHORT).show()

                Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 3000)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}