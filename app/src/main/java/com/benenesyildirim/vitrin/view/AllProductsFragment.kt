package com.benenesyildirim.vitrin.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.benenesyildirim.vitrin.R
import com.benenesyildirim.vitrin.adapter.ProductsAdapter
import com.benenesyildirim.vitrin.refrofit.responsemodel.Product
import kotlinx.android.synthetic.main.fragment_all_products.view.*

class AllProductsFragment : Fragment() {

    private lateinit var producList: List<Product>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_all_products, container, false)

        if (arguments != null) {
            val productListJson = arguments?.getString("products", "")
            producList = Gson().fromJson(productListJson, Array<Product>::class.java).toList()

            setProductList(view)
        }

        view.back_btn_all_products.setOnClickListener {
            Navigation.findNavController(view).popBackStack()
        }

        return view
    }

    private fun setProductList(view: View) {
        view.all_products_rv.layoutManager = GridLayoutManager(requireContext(), 2)
        view.all_products_rv.adapter = ProductsAdapter(producList)
    }
}