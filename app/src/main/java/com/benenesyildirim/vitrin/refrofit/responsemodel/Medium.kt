package com.benenesyildirim.vitrin.refrofit.responsemodel

data class Medium(
    val height: Int,
    val url: String,
    val width: Int
)