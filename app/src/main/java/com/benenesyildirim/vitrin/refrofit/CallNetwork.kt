package com.benenesyildirim.vitrin.refrofit

import com.benenesyildirim.vitrin.refrofit.responsemodel.MainData
import retrofit2.Call
import retrofit2.http.GET

interface CallNetwork {

    @GET("discover")
    fun getDataList(): Call<MainData>
}