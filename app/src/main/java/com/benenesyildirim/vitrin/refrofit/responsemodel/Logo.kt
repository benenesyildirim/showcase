package com.benenesyildirim.vitrin.refrofit.responsemodel

data class Logo(
    val height: Int,
    val medium: Medium,
    val thumbnail: Thumbnail,
    val url: String,
    val width: Int
)