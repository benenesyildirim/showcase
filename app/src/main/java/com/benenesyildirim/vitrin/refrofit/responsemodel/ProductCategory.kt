package com.benenesyildirim.vitrin.refrofit.responsemodel

data class ProductCategory(
    val id: Int,
    val name: String,
    val order: Int,
    val parent_category: ParentCategory,
    val parent_id: Int
)