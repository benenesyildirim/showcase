package com.benenesyildirim.vitrin.refrofit.responsemodel

data class PopularProducts(
    val images: List<İmage>,
)