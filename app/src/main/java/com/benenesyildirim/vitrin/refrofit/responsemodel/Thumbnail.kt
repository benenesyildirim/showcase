package com.benenesyildirim.vitrin.refrofit.responsemodel

data class Thumbnail(
    val height: Int,
    val url: String,
    val width: Int
)