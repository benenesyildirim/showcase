package com.benenesyildirim.vitrin.refrofit.responsemodel

data class Cover(
    val height: Int,
    val medium: Medium,
    val thumbnail: Thumbnail,
    val url: String,
    val width: Int
)