package com.benenesyildirim.vitrin.refrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


const val baseURL = "https://www.vitrinova.com/api/v2/"
class ApiBaseCall {

    companion object{
        fun getData(): Retrofit{
            return Retrofit.Builder().baseUrl(baseURL).addConverterFactory(GsonConverterFactory.create()).build()
        }
    }
}